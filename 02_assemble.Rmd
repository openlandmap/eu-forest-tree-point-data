---
title: "Harmonizing Tree Species Occurrence Data - Assemble"
author: "Johannes Heisig"
date: "September 24th, 2020"
output: 
 rmarkdown::html_document:
    theme: flatly
    highlight: tango
    toc: true
    toc_float:
      collapsed: false
      smooth_scroll: true
    toc_depth: 2
link-citations: yes
toc: yes
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE, warning=FALSE, cache=TRUE}
knitr::opts_chunk$set(cache = TRUE, include = FALSE)
outdir = "~/PhD_home/repos/eu-forest-tree-point-data/"
setwd(outdir)
```

In the following, previously downloaded raw tree species occurrence data is cleaned up and unified. Often existing information can be transferred, sometimes information needs to be joined or added manually.

```{r packages, message=FALSE, warning=FALSE}
library(tidyverse)
select <- dplyr::select  # avoid masking
library(rgbif)
library(mapview)
library(sf)
library(terra)
library(raster)
library(data.table)
```

# 1. Target tree species names

Import the list of required tree species (Google sheet, downloaded as .csv). It includes 87 of the most common tree species in Europe.

```{r gs, warning=FALSE}
gs_ForSpec_list <- read.csv("Potential_natural_vegetation_global_European_forest_species.csv")
gs_ForSpec_list %>% pull(Names_2) %>% unique()
```

# 2. GBIF

The [Global Biodiversity Information Facility](https://www.gbif.org/) provides over two million spatial occurrence points for our desired species. They can be accessed via the online tool or the `rgbif` package.

## 2.1 Import

Import RDS-file created from original GBIF download earlier. It contains 18 out of 241 variables. See `01_download.Rmd` for download preferences of GBIF data.

```{r gbif-read-rds}
gbif_data <- readRDS("gbif_data.rds") %>% glimpse()
```

## 2.2 Filter

As the GBIF data base is a collection of numerous data set, each following their own sampling and documentation standards, careful selection of observations is advised. Hence, obtained points are filtered with respect to basic quality requirements. This is achieved by excluding...

a)  undesired species
b)  occurrences which have `GeospatialIssues`
c)  occurrences that have one of the following issues:

-   `PRESUMED_SWAPPED_COORDINATE`
-   `ZERO_COORDINATE`

d)  occurrences which are not on the rank of *species*, *subspecies* or *genus*.
e)  occurrences where coordinates are missing
f)  occurrences where location error is greater than 5000 meters

```{r gbif-filter}
issues_to_exclude <- c('PRESUMED_SWAPPED_COORDINATE',
                       'ZERO_COORDINATE') %>% paste(collapse = "|")

gbif_sub <- gbif_data %>% 
  filter(taxonKey %in% gs_ForSpec_list$taxonKey,
         hasGeospatialIssues == FALSE,
         str_detect(issue, issues_to_exclude) == FALSE,
         taxonRank %in% c("SPECIES", "SUBSPECIES", "GENUS"),
         !is.na(decimalLongitude) & !is.na(decimalLatitude),
         coordinateUncertaintyInMeters < 5000)
```

That leaves us with `r nrow(gbif_sub)` occurrences.

Adjust column names to fit a consistent naming scheme:

```{r gbif-renaming}
gbif_sub <- gbif_sub %>% rename('country' = countryCode, 
                                'scientific_name' = scientificName,
                                'gbif_taxon_key' = taxonKey, 
                                'gbif_genus_key' = genusKey,
                                'taxon_rank' = taxonRank, 
                                'location_accuracy' = coordinateUncertaintyInMeters,
                                'dataset_info' = datasetName)
```

Remove variables that have fulfilled their filtering purpose.

```{r gbif-delete-cols}
gbif_sub <- gbif_sub %>% 
  mutate(accessed_through = 'GBIF') %>% 
  select(-c(gbifID, recordedBy, hasGeospatialIssues))

names(gbif_sub)
```

## 2.3 Add citations

Extract text citations for each GBIF data set and list them in a look-up table.

```{r gbif-citationsTable}
cit <- tibble(datasetKey = unique(gbif_sub$datasetKey)) %>% 
  mutate(citation = sapply(datasetKey, function(x) gbif_citation(x)[[1]]$text),
         citation = sub(".*(http.*doi.org.*) accessed.*", "\\1", x = citation)) %>%
  glimpse()
```

Join them with the occurrence data set.

```{r gbif-join-citations}
gbif_sub <- cit %>% 
  merge(gbif_sub, ., by = 'datasetKey', all = F) %>%
  mutate(citation = unname(citation)) %>% 
  select(-datasetKey)
```

## 2.4 Quality flags

Add quality flags based on the GBIF "issues"-column. `0` will indicate the absence of relevant issues.

Show all existing issue categories and their frequencies:

```{r gbif-view-issues}
all_issues <- unique(gbif_sub$issue) %>% 
  str_split(';') %>% 
  unlist() %>% 
  unique() %>% 
  sort()

issue_counts = sapply(all_issues, function(x) str_count(gbif_sub$issue, x)) %>% 
  colSums() %>% 
  as.data.frame() %>% mutate(issue = rownames(.)) 

```

Flags can indicate potential problems with a data point and will later help making good data selection decisions, depending on the research goal.

Flags to be added:

**flag_location_issue** = 1 if any of the following issues is present:

-   `COORDINATE_PRECISION_INVALID`
-   `COORDINATE_REPROJECTION_SUSPICIOUS`
-   `COORDINATE_ROUNDED`
-   `COORDINATE_UNCERTAINTY_METERS_INVALID`
-   `GEODETIC_DATUM_INVALID`

**flag_location_issue** = 2 if `location_accurracy` is worse than 500 meters.

**flag_date_issue** = 1 if any of the following issues is present:

-   `RECORDED_DATE_INVALID`
-   `RECORDED_DATE_UNLIKELY`

```{r gbif-apply-flags}
# collect issues to mark
loc_flag <- c('COORDINATE_PRECISION_INVALID',
              'COORDINATE_REPROJECTION_SUSPICIOUS',
              'COORDINATE_UNCERTAINTY_METERS_INVALID',
              'GEODETIC_DATUM_INVALID') %>% 
  paste(collapse = "|")

date_flag <- c('RECORDED_DATE_INVALID',
               'RECORDED_DATE_UNLIKELY') %>% 
  paste(collapse = "|")

# assign flags
gbif_sub <- gbif_sub %>% 
  mutate(flag_location_issue = case_when(str_detect(issue, loc_flag) ~ 1, 
                                         location_accuracy > 500 ~ 2,
                                         TRUE ~ 0),
         flag_date_issue = case_when(str_detect(issue, date_flag) ~ 1,
                                     TRUE ~ 0)) %>% 
  select(-issue)

# check how many points have flags now
table(gbif_sub$flag_location_issue, gbif_sub$flag_date_issue)

# Add empty columns so table is compatible with other tables later
gbif_sub$eoo <- NA    # Extent Of Occurrence
gbif_sub$dbh <- NA    # Diameter Breast Height
gbif_sub$lc1 <- NA    # Land Cover 1
gbif_sub$lc2 <- NA    # Land Cover 2
```

## 2.5 Create spatial object

Convert table to `sf` object and transform to ETRS 89.

```{r gbif-make-sf}
gbif_etrs <- gbif_sub %>% 
  st_as_sf(coords = c('decimalLongitude','decimalLatitude'), crs=4326) %>%
  st_transform(crs = 3035) %>%
  mutate(easting = st_coordinates(.)[,1], 
         northing = st_coordinates(.)[,2])

# arrange column order
gbif_etrs <- gbif_etrs %>% 
  select(easting, northing, country, species, genus, 
         scientific_name, gbif_taxon_key, gbif_genus_key,
         taxon_rank, year, accessed_through, dataset_info, 
         citation, license, location_accuracy, flag_location_issue, 
         flag_date_issue, eoo, dbh, lc1, lc2, geometry)
```

```{r gbif-clean, include=FALSE}
rm(gbif_data, gbif_sub, cit) 

write_rds(gbif_etrs, "gbif_data_harmonized.rds", compress = "gz")
```

# 3. EU-Forest

The EU-Forest data set ([Mauri et al., 2017](https://www.nature.com/articles/sdata2016123#Sec3)) includes slightly under 600k occurrence points, most of which correspond to our desired species list. A large portion of the data comes from national forest inventory plots. It is much less complex than GBIF data.

## 3.1 Import & filter

First import, than summarize Diameter Breast Height in one column. New column (`dbh`) will indicate whether EU-Forest observations have have trees thinner 12 cm (= `1`), thicker 12 cm (= `2`) or both (= `3`).

```{r import-EUF, warning=F}
EUF_data_original <- read_rds("EU-Forest_data_original.rds")

# NAs are expressed as -9999
euf_data <- EUF_data_original %>%
  na_if(-9999) %>%
  rename(country = "COUNTRY", species = "SPECIES NAME",
         dbh1 = "DBH-1", dbh2 = "DBH-2")%>%
  replace_na(list(dbh1 = 0, dbh2 = 0)) %>% 
  mutate(dbh2 = dbh2 * 2,
         dbh = dbh1 + dbh2,
         dbh = na_if(dbh, 0)) %>% 
  select(-c(dbh1, dbh2))
   
table(euf_data$dbh, useNA = 'if')
```

First we check if all desired species appear in the EU-Forest data. Because EU-Forest data contains more than our desired tree species we then subset by using our species list.

```{r check-EUF, warning=F}
tibble(target_species = gs_ForSpec_list$Names_2) %>% 
  mutate(in_EUForest = target_species %in% unique(euf_data$species)) %>% 
  filter(in_EUForest == FALSE)       

# exclude undesired species
euf_data <- euf_data %>% 
  filter(species %in% gs_ForSpec_list$Names_2) %>% 
  glimpse()
```

## 3.2 Supplementary information

Create and fill additional columns to later match the prepared GBIF data. Missing information can be derived from existing columns, matched from the GBIF data set, or filled in by hand.

```{r add-columns, warning=F}
euf_data$genus <- sub(" .*" , "", euf_data$species)

# look-up from reference species list
euf_data$scientific_name <- gs_ForSpec_list$Scientific_Name[match(euf_data$species,
                                                              gs_ForSpec_list$Names_2)]
euf_data$gbif_taxon_key <- gs_ForSpec_list$taxonKey[match(euf_data$species,
                                                         gs_ForSpec_list$Names_2)]

# dataset_info specifies in which of EU-Forest's source data bases (NFI, FF, BS) 
# the point is recorded. can be multiple.
euf_data <- euf_data %>% 
  mutate(NFI = case_when(NFI == 1 ~ "NFI", TRUE ~ ""),
         FF  = case_when(FF == 1 ~ "FF", TRUE ~ ""),
         BS  = case_when(BS == 1 ~ "BS", TRUE ~ ""),
         dataset_info = paste(NFI, FF, BS, sep = "-", recycle0 = T) %>% 
           gsub("^-|--|-$|","", .) %>% 
           sub("NFIBS", "NFI-BS", .),
         taxon_rank = "SPECIES",
         year = NA,
         accessed_through = "EU-Forest") %>% 
  select(-c(NFI, FF, BS))

# info from https://www.nature.com/articles/sdata2016123#Sec5
euf_data$citation <- 'https://doi.org/10.1038/sdata.2016.123'   # Mauri et al. (2017) 
euf_data$license <- 'CC BY 4.0'              

euf_data$location_accuracy <- 500   # assumed, as points are centroids of 1x1km grid cells
euf_data$flag_location_issue <- 0   # no info on location problems
euf_data$flag_date_issue <- 1       # no date info present
euf_data$lc1 <- NA
euf_data$lc2 <- NA
```

Fetch GBIF genus keys where they are still missing. We can use `rgbif` to look up an individual species and extract missing information for non-GBIF data.

```{r missing-genusKeys, warning=F}
# match macro group (gbif genusKey) from gbif data
euf_data$gbif_genus_key <- gbif_etrs$gbif_genus_key[match(euf_data$species, gbif_etrs$species)]

# how many are still missing?
sum(is.na(euf_data$gbif_genus_key)) 

# which ones?
euf_data$scientific_name[is.na(euf_data$gbif_genus_key)] %>% 
  unique() 

# taxon keys of species with missing genus key
missing_gnk <- euf_data %>% 
  filter(is.na(euf_data$gbif_genus_key)) %>% 
  select(scientific_name, gbif_taxon_key) %>% 
  unique() %>% 
  na.omit()

# search GBIF by taxon key
s1 <- map_df(missing_gnk$gbif_taxon_key, 
                    function(x) occ_search(taxonKey = x,
                                           fields = c("scientificName",
                                                      "taxonKey",
                                                      "genusKey"))$data) %>% 
  unique() %>%
  filter(taxonKey %in% missing_gnk$gbif_taxon_key)

# search GBIF by name
s2 <- map_df(missing_gnk$scientific_name, 
                    function(x) occ_search(scientificName = x,
                                           fields = c("scientificName",
                                                      "taxonKey",
                                                      "genusKey"))$data) %>% 
  unique() %>%
  filter(taxonKey %in% missing_gnk$gbif_taxon_key)

# combine items found in either one of the searches
lookup_gns <- rbind(unique(s1), unique(s2)) %>% 
  unique()  

# insert GBIF genusKey where missing
euf_data$gbif_genus_key[
  is.na(euf_data$gbif_genus_key)] <- lookup_gns$genusKey[
    match(euf_data$gbif_taxon_key[is.na(euf_data$gbif_genus_key)], 
          lookup_gns$taxonKey)]   

(still_missing <- sum(is.na(euf_data$gbif_genus_key)))  # still anything missing?
ifelse(still_missing != 0, 
       euf_data$scientific_name[is.na(euf_data$gbif_genus_key)] %>% unique(),
       paste('All missing genus key were found and filled in!'))
```

## 3.3 Create spatial object

Convert to `sf` object. Coordinates are already in ETRS 89.

```{r euf-sf}
euf_etrs <- euf_data %>% 
  st_as_sf(coords = c('X','Y'), crs=3035, remove = FALSE) %>% 
  rename(easting = X, northing = Y, eoo = EEO)

# arrange column order 
euf_etrs <- euf_etrs %>% 
  select(easting, northing, country, species, genus, scientific_name, 
         gbif_taxon_key, gbif_genus_key, taxon_rank, year, accessed_through,
         dataset_info, citation, license, location_accuracy, 
         flag_location_issue, flag_date_issue, eoo, dbh, lc1, lc2, geometry)
```

```{r euf-clean, include=FALSE}
rm(EUF_data_original,euf_data,s1, s2, missing_gnk, lookup_gns)

write_rds(euf_etrs, "EU-Forest_data_harmonized.rds", compress = "gz")
```

# 4. LUCAS

[Land Use and Cover Area frame Survey data by Eurostat](https://ec.europa.eu/eurostat/web/lucas/overview), conducted in 2018. Observations cover the Euro28 states. Unfortunately its land cover information does not include exact species, but it differentiates between broad-leaved, coniferous and mixed forests. In subcategories, spruce- and pine-dominated stands are separated. All points considered for the final data set should have the general land cover type "C", indicating "woodland".

## 4.1 Import & filter

```{r LUCAS-import}
LUCAS_data_original <- readRDS("LUCAS_data_original.rds")

LUCAS_data <- LUCAS_data_original %>% 
  select(TH_LONG, TH_LAT, GPS_LONG, GPS_LAT, GPS_PROJ, GPS_PREC, 
         SURVEY_DATE, OBS_DIST, OBS_TYPE, NUTS0, LC1, LC2, LC1_SPEC)

# keep only point with land cover clas "CXX" (= woodland)
LUCAS_data <- LUCAS_data %>% 
  filter(!is.na(GPS_LONG),
         str_detect(LC1, "C") == T)
```

## 4.2 Supplementary information

Survey locations are defined by an even 2x2 km grid. Resulting locations are expressed as theoretical coordinates (`TH_LONG` & `TH_LAT`). Most points are sampled in the field, therefore they have `GPS_LONG`, `GPS_LAT` and `GPS_PREC` (precision). Field sample locations always slightly differ from theoretical coordinates. Additionally, inaccessible point are sampled remotely (image interpretation), hence they only have an entry for theoretical coordinates, not for GPS coordinates.

```{r add-info-LUCAS}
# 1. lon/lat: either from GPS or from theoretical coordinates (remote observation)
# 2. year: extract year from survey date
# 3. genus: get spruce- or pine-dominated points from LC1
# 4. gbif_genus_key: match key according to genus
# 5. insert dataset, citation and license by hand
# 6. transfer location accurracy from GPS precision
# 3. fill with NA where information is missing

LUCAS_data <- LUCAS_data %>% 
  mutate(lon = case_when(GPS_LONG > 88 ~ TH_LONG, TRUE ~ as.numeric(GPS_LONG)),
         lat = case_when(GPS_LAT > 88 ~ TH_LAT, TRUE ~ as.numeric(GPS_LAT)),
         year = as.numeric(paste0("20", sub( ".*/*/", "", SURVEY_DATE))),
         species = NA,
         genus = case_when(LC1 %in% c("C21", "C31") ~ "Picea",
                           LC1 %in% c("C22", "C32") ~ "Pinus"),
         scientific_name = NA,
         gbif_taxon_key = NA,
         taxon_rank = case_when(!is.na(genus) ~ "GENUS"),
         accessed_through = 'LUCAS',
         dataset_info = 'LUCAS',
         citation = "EUROSTAT 2017. Land Cover/Use Statistics (LUCAS) Database. EUROSTAT, Luxembourg.URL http://ec.europa.eu/eurostat/web/lucas/data/database [accessed on 01.08.2020]." ,
         license = 'CC BY 4.0',
         location_accuracy = case_when(GPS_PREC < 8888 ~ GPS_PREC, TRUE ~ 0),
         eoo = NA,
         dbh = NA,
         lc2 = na_if(LC2, "8")) %>% 
  rename(country = NUTS0, lc1 = LC1) %>% 
  select(-c(TH_LONG, TH_LAT, GPS_LONG, GPS_LAT, SURVEY_DATE, GPS_PREC, LC2, LC1_SPEC))

LUCAS_data$gbif_genus_key <- gbif_etrs$gbif_genus_key[match(LUCAS_data$genus, gbif_etrs$genus)]
```

Check frequencies of genus in LUCAS data:

```{r lucas-forest-types}
table(LUCAS_data$genus, useNA = 'if')
```

## 4.3 Quality flags

LUCAS field data has some data quality indicators that can be translated to our flag system.

Flags to be added:

**flag_location_issue** = 1 if any of the following is true:

-   `GPS_PROJ` == 2 (GPS problem)
-   `GPS_PREC`/`location_accuracy` \> 500 meters
-   `OBS_TYPE` == 2 (observation further than 100 meters distance)

**flag_date_issue** = 0 (all date entries are assumed to be correct)

*Also: exclude points with location accuracy worse than 5000 meters (just 1 observation).*

```{r flag-LUCAS}
LUCAS_data <- LUCAS_data %>% 
  mutate(flag_location_issue = case_when(GPS_PROJ == 2 ~ 1,
                                         location_accuracy > 500 ~ 2,
                                         OBS_TYPE == 2 ~1,
                                         TRUE ~ 0),
         flag_date_issue = 0) %>% 
  filter(location_accuracy < 5000) %>% 
  select(-c(GPS_PROJ, OBS_DIST, OBS_TYPE))
```

## 4.4 Create spatial object

Convert to `sf` object and transform projection to ETRS89.

```{r lucas-sf}
LUCAS_data <- LUCAS_data %>% 
  st_as_sf(coords = c('lon','lat'), crs=4326, remove = FALSE)

LUCAS_etrs <- LUCAS_data %>%
  st_transform(3035) %>% 
  mutate(easting = st_coordinates(.)[,1], 
         northing = st_coordinates(.)[,2]) 

# arrange columns
LUCAS_etrs <- LUCAS_etrs %>% 
  select(easting, northing, country, species, genus, scientific_name, 
         gbif_taxon_key, gbif_genus_key, taxon_rank, year, accessed_through, 
         dataset_info, citation, license, location_accuracy, 
         flag_location_issue, flag_date_issue, eoo, dbh, lc1, lc2, geometry,
         -lon, -lat)
```

```{r lucas-clean, include=FALSE}
rm(LUCAS_data, LUCAS_data_original)

write_rds(LUCAS_etrs, "LUCAS_data_harmonized.rds", compress = "gz")
```

# 5. Assemble

## 5.1 Bind

```{r bind-tables}
# check if tables can be bound
identical(names(euf_etrs), names(gbif_etrs))
identical(names(gbif_etrs), names(LUCAS_etrs))

harmonized <- list(gbif_etrs, euf_etrs, LUCAS_etrs) %>% 
  rbindlist() %>% 
  st_as_sf()

# drop irrelevant attribute
attr(harmonized,'agr') <- NULL
```

```{r bind-clean, include=FALSE}
rm(gbif_etrs, euf_etrs, LUCAS_etrs)
```

Sort out differences in country names so they all match ISO standard. Also, add a unique ID number for each occurrence.

```{r rename-countries}
length(unique(harmonized$country))

# sort out different country naming conventions
harmonized <- harmonized %>%
  mutate(country = case_when(country == 'Austria' ~ 'AT',
                             country == 'Belarus' ~ 'BB',
                             country == 'Belgium' ~ 'BE',
                             country == 'Bulgaria' ~ 'BG',
                             country == 'Croatia' ~ 'HR',
                             country == 'Cyprus' ~ 'CY',
                             country == 'Czech Rep' ~ 'CZ',
                             country == 'Denmark' ~ 'DK',
                             country == 'Estonia' ~ 'EE',
                             country == 'Finland' ~ 'FI',
                             country == 'France' ~ 'FR',
                             country == 'Germany' ~ 'DE',
                             country == 'Greece' ~ 'GR',
                             country == 'Hungary' ~ 'HU',
                             country == 'Ireland' ~ 'IE',
                             country == 'Italy' ~ 'IT',
                             country == 'Latvia' ~ 'LV',
                             country == 'Lithuania' ~ 'LT',
                             country == 'Moldova' ~ 'MD',
                             country == 'Netherlands' ~ 'NL',
                             country == 'Norway' ~ 'NO',
                             country == 'Polland' ~ 'PL',
                             country == 'Portugal' ~ 'PT',
                             country == 'Romania' ~ 'RO',
                             country == 'Slovakia' ~ 'SK',
                             country == 'Slovenia' ~ 'SI',
                             country == 'Spain' ~ 'ES',
                             country == 'Sweden' ~ 'SE',
                             country == 'Switzerland' ~ 'CH',
                             country == 'United Kingdom' ~ 'UK',
                             TRUE ~ country))

t <- table(harmonized$country, useNA = 'if') / 1000

t[t>1]  %>% 
  sort() %>% 
  barplot(horiz = F, las=2, cex.names = 0.8,
          ylab = "Observations / 1000",
          main = "Occurrence points per EU country")
```

## 5.2 Overlay supplementary data

Below, we overlay point locations with several data sets. Additional information will give users more flexibility and options for filtering. This way tree species points can be selected according to the user's specific needs more easily. For example, points which fall into a forest land cover class may be more useful for some applications than those within urban areas.

### EU land mask

A 30 meter European land mask from [Pflugmacher et al. (2019)](https://doi.org/10.1016/j.rse.2018.12.001) is used to identify points which fall into e.g. water bodies. This can be the case if both the observation lies in close proximity to water and has poor location accuracy. The user may later exclude points outside this land mask (= `NA`) e.g. to avoid introducing spectral signatures of water into model training. Points on land will have numeric codes each representing a country.

```{r landmask, warning=FALSE}
# create terra objects for fast extraction
landmask <- "enrich_data/country_mask_eu_30m.tif" %>% rast()
harm_vect <- vect(harmonized)

# extract
harmonized$landmask_country = terra::extract(landmask, harm_vect)[,2]
harmonized$landmask_country[is.nan(harmonized$landmask_country)] <- NA

table(harmonized$landmask_country, useNA = 'if')
```

```{r landmask-clean, include=FALSE}
rm(landmask)
```

### CORINE Land Cover

Corine is a pan-European land cover data set at 100 meter cell size (can also be obtained as vector data with a 25 ha minimum mapping unit). The data is free to [download here](https://land.copernicus.eu/pan-european/corine-land-cover/clc2018). Land cover information may be used filter for occurrence points which fall e.g. in urban or forest areas.

```{r read-corine, warning=FALSE}
corine <- "enrich_data/U2018_CLC2018_V2020_20u1.tif" %>% rast()
#harm_vect <- vect(harmonized)
 
# get value-label pairs from clc color table
x <- readLines("enrich_data/corine_colortable.txt") %>% as.data.frame()

corine_legend <- str_split_fixed(x[,1], pattern = ' ', n = 8)[,c(1,6,8)] %>% 
  data.frame() %>% 
  set_names(c("Value","CLC","Name")) 

knitr::kable(corine_legend[23:25,]) 
```

Reclassify the Corine data so raster cells contain corresponding land cover codes.

```{r reclassify-corine}
# reclassify CLC so it contains land cover codes
corine <- corine_legend %>% 
  select(Value, CLC) %>%
  unlist() %>% 
  as.numeric() %>% 
  matrix(ncol=2) %>% 
  classify(corine, .)
```

Extract raster values and show the ones occurring most often.

```{r extract-corine}
# extract 
harmonized$corine = terra::extract(corine, harm_vect)[,2] 

table(harmonized$corine, useNA = 'if') %>% 
  sort() %>% 
  tail()
```

```{r corine-clean, include=FALSE}
rm(corine, x, corine_legend)
```

As expected, most points have a forest land cover (code 3XX). Many points, however, show agricultural land cover (code 2XX). One of the reasons for this behavior is, that many points are centroids of a larger grid cell (e.g. 1000x1000 meter), so they do not correspond to a single field observation.

### VIIRS Nighttime Lights

Light pollution is observed by the VIIRS sensor and prepared for [download](https://eogdata.mines.edu/download_dnb_composites.html) by the Earth Observation Group at Payne Institute for Public Policy. Here annual average values for the year 2020 are extracted at all occurrence points. This information may be used as a proxy for human influence in an area. Filtering data above a certain threshold (e.g. 1) can exclude observations close to illuminated human structures.

```{r extract-lights}
nightlights <- rast("enrich_data/VNL_v2_npp_2020_global_vcmslcfg_c202102150000.average_masked.tif")
#nightlights = crop(nightlights, ext(-26,40,25,72))

# transform points to WGS (less computation than projecting the raster to ETRS)
harm_vect_wgs <- harmonized %>% 
  st_transform(4326) %>% 
  vect()

harmonized$nightlights = terra::extract(nightlights, harm_vect_wgs)[,2] 
harmonized$nightlights[is.nan(harmonized$nightlights)] <- NA

# how many points are close to illuminated structures?
sum(harmonized$nightlights > 1, na.rm=T)
```

```{r lights-clean, include=FALSE}
rm(nightlights)
```

### Extract GEDI Canopy Height

GEDI data collected on-board the International Space Station covers the Earth between 52 degrees latitude north and south. Its waveform LiDAR enables high resolutions in the vertical domain. Recently spatially consistent data sets were derived from GEDI point data, allowing, besides other applications, the estimation of tree canopy height globally. OpenGeoHub published such a data set ([here](https://zenodo.org/record/4057883#.X3WrHO2xVVI)), which is ready for [download here](https://zenodo.org/record/4057883/files/dtm_canopy.height_glad.umd_m_30m_0..0cm_2019_eumap_epsg3035_v0.1.tif?download=1).

```{r extract-GEDI-canopy}
can_height <- rast("enrich_data/dtm_canopy.height_glad.umd_m_30m_0..0cm_2019_eumap_epsg3035_v0.1.tif")
  
harmonized$canopy_height = terra::extract(can_height, harm_vect)[,2] 
```

```{r gedi-clean, include=FALSE}
rm(can_height)
```

### Natura2000 sites

Natura 2000 is the European Union's key instrument to protect biodiversity. It is an ecological network of protected areas, set up to ensure the survival of Europe's most valuable species and habitats. GIS vector layers with boundaries of protected areas are free to [download here](https://www.eea.europa.eu/data-and-maps/data/natura-11).

```{r extract-Natura2000, message=FALSE, warning=FALSE}
n2000 <- st_read("enrich_data/Natura2000_end2020.gpkg", quiet = T) %>% 
  select(SITECODE)

harmonized <- st_join(harmonized, n2000, join = st_intersects)

harmonized <- harmonized %>% rename(natura_2000 = SITECODE)

# which percentage of points fall inside Natura2000 sites?
sum(!is.na(harmonized$natura_2000))/nrow(harmonized)*100
```

```{r n200-clean, include=FALSE}
rm(n2000)
```

### Extent of Occurrence (EOO)

EOOs describe a rough distribution or natural range of a species. It can help to know, wether an individual occurs within its natural geographical range or not. This approach was implemented in the EU-Forest data set [(Mauri et al., 2017)](https://www.nature.com/articles/sdata2016123). Here, `1` indicates a point falls inside it's species geographic range.

```{r eoos}
# glue together many single polygon shapefiles
eoos <- list.files("enrich_data/EOOs/", pattern=".shp", full.names = T) %>% 
  lapply( function(x) st_read(x,quiet=T)) %>% 
  do.call(rbind, .) %>% 
  st_transform(3035)

# example EOO
eoos %>% 
  filter(Name == "Fagus sylvatica") %>% 
  mapview(color ="red", alpha.regions = 0, lwd = 2,
          map.types = "CartoDB.DarkMatter", legend = F)

# are there any species, where no EOO polygon exists?
unique(harmonized$species)[is.na(match(unique(harmonized$species), unique(eoos$Name)))]

# check if species occurrence points fall in corresponding EOO polygon
for (n in unique(harmonized$species)){
  if (n %in% eoos$Name){
      h <- harmonized[which(harmonized$species == n),]
      e <- eoos %>% filter(Name == n)
      harmonized$eoo[which(harmonized$species == n)] <- lengths(st_intersects(h,e)) 
  }
}

table(harmonized$eoo, useNA = 'if')
```

## 5.3 Detect duplicate locations

Quick and dirty approach to check for duplicate locations. Points e.g. from national forest inventories represent the centroid of a large grid cell. A point can occur multiple times, once for each species present in the grid cell. Some modeling task might prefer locations where only one species is observed. Because geo-spatial operations on several million points can take a long time, a simple matching exercise of concatenated strings of the coordinates is performed.

```{r loc_frequency}
harmonized$eastnorth <- str_c(harmonized$easting, harmonized$northing, sep = ' - ') 

freq_location <- harmonized$eastnorth %>% 
  table(dnn='eastnorth') %>% 
  as.data.frame(responseName = 'freq_location',
                stringsAsFactors = F)

harmonized <- merge(harmonized, freq_location, by="eastnorth")
harmonized$eastnorth <- NULL

# how many locations have more than 1 observation
sum(harmonized$freq_location > 1)
```

## 5.4 Remove duplicate observations

Identify duplicates, where all variable match (including location, year and species).

```{r duplicates}
# find duplicates 
harmonized.dups <- st_drop_geometry(harmonized) %>% 
  duplicated()

table(harmonized.dups)

# remove
harmonized <- harmonized %>% filter(!harmonized.dups)
```

# 6. Write

Write final data set to disk.

```{r write-final-table}
# first add unique ID column
harmonized <- harmonized %>%
  mutate(id = 1:nrow(.)) %>%
  select(id, everything())

write_rds(harmonized, "tree_species_occ_harmonized_final.rds", compress = "gz")
write_csv(harmonized, "tree_species_occ_harmonized_final.csv")
st_write(harmonized, "tree_species_occ_harmonized_final.gpkg", delete_dsn = T)
```

Write list of citations separately.

```{r}
writeLines(unique(harmonized$citation), "citations.txt")
```


# 7. Explore

The harmonized data set now has `r nrow(harmonized)` occurrences.

## 7.1 Spatial distribution

Check spatial density of occurrence points:

```{r occ-density, warning=FALSE, message=FALSE}
# create empty raster over Europe
empty_grid <- raster(xmn = -25, ymn = 25, xmx = 40, ymx = 72, 
                     crs = CRS("+init=EPSG:4326"), vals = 0,
                     resolution = 1)

# count occurrences per cell
occ_density <- harmonized %>% 
  st_geometry() %>% 
  st_transform(4326) %>% 
  as_Spatial() %>% 
  coordinates() %>% 
  rasterize(x = ., y = empty_grid, fun = 'count', background = 0)

mapview(occ_density, at = 10^c(1:5))
```

## 7.2 Summary

Overall summary:

```{r summary}
summary(harmonized)
```

When was the data collected?

```{r year}
# all years
ggplot(harmonized, aes(x=year)) +
  geom_histogram(stat="count") +
  theme_minimal()

# since 2000
filter(harmonized, year >= 2000) %>%
  ggplot(aes(x=year)) +
  geom_histogram(stat="count") + theme_minimal()

```

Which data sources contribute how much?

```{r contributions}
t <- table(harmonized$accessed_through) %>% print()
barplot(prop.table(t))
```

## 7.3 Filter example

In this example somebody wants to model the distribution of pine species in Germany. Relevant training points should be observed after 2010, should have no issues regarding location or date.

```{r example}
my_pines <- harmonized %>% 
  filter(genus == "Pinus",                            # all pine species
         country == "DE",                             # in Germany
         accessed_through %in% c("GBIF",              # data from GBIF or EU-Forest
                                 "EU-Forest"),
         year > 2010,                                 # only recent observations
         flag_location_issue == 0,                    # no location problems
         flag_date_issue == 0)                        # no date problems

# how many occurrences are left after filtering?
nrow(my_pines)

mapview(my_pines, 
        zcol = "species", 
        color=NULL, 
        cex=3,
        alpha.regions = 0.3,
        map.types = "CartoDB.DarkMatter",
        layer.name = "Pine species in GER")
```
