# A pan-European forest tree species occurrence point data set

This repository stores code for the compilation of analysis-ready point data with the purpose of forest tree species and Potential Natural Vegetation mapping. It is spatially limited to EU boundaries. 

Data originally comes from
 - Global Biodiversity Information Facility ([GBIF](https://www.gbif.org/))
 - EU-Forest ([Mauri et al. 2017](https://www.nature.com/articles/sdata2016123#Sec3))
 - Land Use/Cover Area Survey ([LUCAS](https://ec.europa.eu/eurostat/web/lucas))
  
 Resulting data set has ~ **2.4 million points** and can be [**downloaded for free**](https://zenodo.org/record/5524611#.YXF6TptCS-o).

### Preview
<img src="001_preview_treespeciespoints.PNG" width="600">

Tree species points in mainland Europe (without Spanish/Portugese islands and Iceland). Species are assigned random colors.
Note: some points may not be visible due to overlay.

### Main variables
- **id** = unique point identifier
- **easting** = x coordinate
- **northing** = y coordinate
- **country** = ISO country code
- **species** = Latin species name
- **genus** = genus name
- **scientific_name** = long species name
- **gbif_taxon_key** = taxon key from GBIF
- **gbif_genus_key** = genus key from GBIF
- **geometry** = point geometry in ETRS89 / LAEA Europe

### Meta information

Variables listed below serve as supplementary information and may be useful for data set filtering. It enable quick and easy tailoring of the data to user specific needs.

- **year** = year of observation
- **taxon_rank** = species or genus
- **accessed_through** = database through which data was accessed (GBIF, LUCAS, EU-Forest)
- **dataset_info** = data set name (individual sub-data-set)
- **citation** = DOI citation of the individual data set
- **license** = distribution license
- **location_accuracy** = spatial accuracy of observation (meters)
- **flag_location_issue** = known location issues present
- **flag_date_issue** = known date issues present
- **eoo** = Extent of occurrence (applying the concept of natural geographical range used for the EU-Forest data set (Mauri et al., 2017) to all other data points. 1 = point inside species range; 0 = point outside; NA = EOO polygon not available for this species)
- **dbh** = Diameter Breast Height (only recorded for observations from the EU-Forest data set)
- **lc1** = LUCAS land cover type 1 (only recorded for observations from LUCAS data)
- **lc2** = LUCAS land cover type 2 (only recorded for observations from LUCAS data)
- **landmask_country** = land mask overlay 30 meters (NA = not on land)
- **corine** = CORINE 2018 land cover type (extracted from the 100 meter raster data set; see *corine_colortable.txt* for a class overview)
- **nightlights** = light pollution observed by VIIRS (proxy for remoteness / distance to human structures)
- **canopy_height** = canopy height derived from GEDI waveform LiDAR point data
- **natura_2000** = Natura 2000 site code (if a point falls inside a protected area (GIS-layer) this variable contains the site identification code; all sites can be explored on an interactive map)
- **freq_location** = number of points with identical location (in some cases one location has multiple observation, differing in species and/or year. This may lead to difficulties in certain modeling tasks)

### Documentation
This data set is accompanied by a more [**detailed documentation**](https://docs.google.com/spreadsheets/d/1WM0BIaVEKxTsCISEaF76RJ8F1iWiZlDfuyQCBiF2Sxw/edit?usp=sharing) describing each variable and its origin. It also includes a list of individual GBIF data set citations. The current version was last updated in October 2021.

### Code
Data processing for this task was executed using the R progamming language. The result is reproducible if one obtains code and input data. The latter can be very large in memory (e.g. *TIF* files covering entire Europe) and are therefore not added to this repository. However, links to all data sources are provided in the R-Markdown files.
